#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
# Para leer por pantalla
import sys

# Cliente UDP simple.

# Dirección IP del servidor.

METODO = str(sys.argv[1])
RECEPTOR = sys.argv[2].split(':')[0]
SERVER = RECEPTOR.split('@')[1]
PORT = int(sys.argv[2].split(':')[1])

if len(sys.argv) != 3:
    sys.exit("Usage: python3 client.py met phod receiver@IP:SIPport")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))

    ACK_FRASE = ('ACK ' + 'sip:' + RECEPTOR + '@' + SERVER + ' SIP/2.0\r\n')
    enviar1 = METODO + ' sip:' + RECEPTOR + ' SIP/2.0'
    print(enviar1)
    my_socket.send(bytes(enviar1, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    # mirar que hay en la primera linea de data.decode
    print('Recibido -- \r\n', data.decode('utf-8'))
    recibido = data.decode('utf-8').split(' ')
    if METODO == 'INVITE':
        if recibido[1] == '100':
            print("Enviamos: " + ACK_FRASE)
            my_socket.send(bytes(ACK_FRASE, 'utf-8') + b'\r\n')
    # ¿QUE OCURRE CON EL BYE?

    print("Terminando socket...")
    print("Fin")
