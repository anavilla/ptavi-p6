#!/usr/bin/python3
# -*- coding: utf-8 -*-
#Ana Villanueva
"""CREO QUE NO FUNCIONA ADECUADAMENTE PUES NO ME SALE NADA EN TERMINAL"""
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import socketserver
import sys
import random
import simplertp


# Realizacion de un diccionario para apartado b
# ¿Como obtener direccion e ip? ¿audio 67876 RTP que es? b

class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            print("El cliente nos manda " + line.decode('utf-8'))
            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break
            recibido = line.decode('utf-8')
            print(recibido)
            METODO = recibido.split(' ')[0]
            print("El cliente nos manda: " + METODO)
            if METODO == 'INVITE':
                UsuarioC = recibido.split(':')[1]
                DireccionC = recibido.split('@')[1]
                suma = str(len("v=0\r\n" + "o= " + UsuarioC + " " + DireccionC + "\r\n"
                               + "s=ana sesion\r\n\ t=0\r\n" + "t=0\r\n" +
                               "m=audio 70397 RTP\r\n"))
                SDP = "Content-Lenght: " + suma + "\r\n\r\n"
                SDP += "v=0\r\n"
                SDP += "o= " + UsuarioC + " " + DireccionC + "\r\n"
                SDP += "s=ana sesion\r\n\ t=0\r\n"
                SDP += "t=0\r\n"
                SDP += "m=audio 70397 RTP\r\n"
                m = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 "
                m += "Ringing\r\n\r\nSIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(bytes(m + SDP, 'utf-8'))

            elif METODO == 'ACK':
                ALEAT = random.randint(0, 1000)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0,
                                      marker=0, ssrc=ALEAT)
                audio = simplertp.RtpPayloadMp3(FICH_AUDIO)
                simplertp.send_rtp_packet(RTP_header, audio,
                                          "127.0.0.1", 23032)
            elif METODO == 'BYE':
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            else:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":
    # Introduccion de valores
    SERVER1 = sys.argv[1]
    SERVERSTR = str(SERVER1)
    # imprimir por pantalla sting
    PORT1 = int(sys.argv[2])
    FICH_AUDIO = sys.argv[3]
    if len(sys.argv) != 4:
        sys.exit("Usage: python3 server.py IP port audio_file")
    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((SERVER1, PORT1), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()
